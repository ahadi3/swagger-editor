'use strict';

window.SwaggerEditor = angular.module('SwaggerEditor', [
  'ngSanitize',
  'ngResource',
  'ui.router',
  'ui.ace',
  'ui.bootstrap',
  'ngStorage',
  'ngSanitize',
  'hc.marked',
  'ui.layout',
  'ngFileUpload',
  'mohsen1.schema-form',
  'jsonFormatter'
]);
