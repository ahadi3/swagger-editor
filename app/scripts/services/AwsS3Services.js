SwaggerEditor.service('awsServices', ['$http','$resource', function($http,$resource) {

	this.getSwaggerSpecs = function(scope) {
				scope.action = 'listObjectVersions';
				this.setupListObjectVersionData(scope);
				this.metaDataList(scope);	
			};
	
	
	this.setupListObjectVersionData = function(scope){
		scope.params = {
						EncodingType: 'url'
				};
	}
	
	this.metaDataList = function(scope) {
		$http.get('config/awsProperties.json').
			success(function(data, status, headers, config) { 
			retrieveSignedURL(scope,data);
			$http.get(scope.url).success(function(data) {
				var latestMetaModels = new Array();
				var allFiles = new Array();
				var x2js = new X2JS();
                var json = x2js.xml_str2json( data );
				for(var i=0;i<json.ListVersionsResult.Version.length ; i++){
				
					if(json.ListVersionsResult.Version[i].IsLatest == "true")
					latestMetaModels.push(json.ListVersionsResult.Version[i].Key);
					allFiles .push(json.ListVersionsResult.Version);
				}
				
				scope.files = latestMetaModels ; 
				sessionStorage.latestFiles=allFiles;
			}).error(function(status, statusText) {
				console.log(status+"..........."+statusText);					
				});
		});			
	};		
 //-------Save a Meta Data. Starts
			
			this.saveYmlData = function(scope,data){
				scope.action = 'putObject';
				this.setupPutData(scope,data.info.title);
				this.persistMetaData(scope,data);
				
			}
			
			this.setupPutData = function(scope,fname) {
				var fname = fname.replace(/ /g, '');
				var uniqueFileName = "SWG-"+fname+".json";				
				scope.params = {
					Key : uniqueFileName,
					ContentType : 'application/json;charset=UTF-8',
					Body : undefined
				};
			}
			
		
	
			this.persistMetaData = function(scope,ymlData) {
				$http.get('config/awsProperties.json').
				  success(function(data, status, headers, config) {
					
					retrieveSignedURL(scope,data);
					postObject(scope,ymlData);
				  });

					
			};
			
			
	
			var retrieveSignedURL = function(scope,data){
			 AWS.config.update({
					accessKeyId : data.accesskey,
					secretAccessKey : data.secretKey
				});
				AWS.config.region = 'eu-west-1';
				var bucket = new AWS.S3({
					params : {
						Bucket : data.domain
					}
				});
				scope.url = bucket.getSignedUrl(scope.action, scope.params);			
			}
			
			var postObject = function(scope,data) {	
				var fname = data.info.title.replace(/ /g, '');
				var fileName = "SWG-"+fname+".json";
						

				$http.put(scope.url,data).success(function(data) {
					alert("file save by name : "+fileName);
					}).error(function(status, statusText) {					
				});				
			};
			
//--------------------------------Retrive a  Oject 

	this.retrieveObject = function(scope,fileName) {
					scope.action = 'getObject';
					var fileVersion;
					session.storage.latestFiles.forEach(function(item) {
 						if(iteam.Key == fileName) {
							fileVersion = item.VersionID;	
							}
					});					
					this.setupGetData(scope,fileName,fileVersion);				
					return this.getObject(scope);					
			}	
	var getFileVersion = function(fileName){
			var fileVersion ;
					for (var i=0;i<sessionStorage.latestFiles.size;i++){
							sessionStorage.latestFiles[i].Key.contains('SWG')
					if(sessionStorage.latestFiles[i].Key == fileName ){
						fileVersion = sessionStorage.latestFiles[i].VersionId ; 
							}
					}	
			return fileVersion 
	}
	this.setupGetData = function(scope,fileName) {
				var uniqueFileName = fileName;
				var versionId = getFileVersion(fileName);
				if (versionId == null || versionId == '') {
					scope.params = {
						Key : uniqueFileName
					};
				} else {
					scope.params = {
						Key : uniqueFileName,
						VersionId : versionId,
						ResponseContentType : 'application/json'
					};
				}

			}	
	this.getAwsUrl= function(scope,data,action){
			 AWS.config.update({
					accessKeyId : data.accesskey,
					secretAccessKey : data.secretKey
				});
				AWS.config.region = 'eu-west-1';
				var bucket = new AWS.S3({
					params : {
						Bucket : data.domain
					}
				});
				return bucket.getSignedUrl(action, scope.params);			
			}
		
	return this ; 	
	
}]);